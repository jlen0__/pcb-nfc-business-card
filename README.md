# PCB NFC Business Card

## CAD Files
(https://365.altium.com/files/F7696009-C609-4CFA-A902-3A159478D28B)

Use the above link to view and download the files from Alitum. ZIP is also stored in this repo.


## Antenna
Made a calculator in a spreadsheet to get the inductance correct for the dimensions. Found some Delhpi Altium has for drawing spirals so used that to get the correct spacing and number for turns. Original uploaded in ZIP, also available here (https://github.com/Altium-Designer-addons/scripts-libraries)


## Encode Card
(https://apps.apple.com/au/app/nfc-tools/id1252962749)
